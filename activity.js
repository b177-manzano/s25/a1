/* 1 */
{
    "_id" : ObjectId("628e015044f9acaee0e60077"),
    "name" : "Apple",
    "color" : "Red",
    "stock" : 20.0,
    "price" : 40.0,
    "supplier_id" : 1.0,
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "US"
    ]
}

/* 2 */
{
    "_id" : ObjectId("628e015044f9acaee0e60078"),
    "name" : "Banana",
    "color" : "Yellow",
    "stock" : 15.0,
    "price" : 20.0,
    "supplier_id" : 2.0,
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "Ecuador"
    ]
}

/* 3 */
{
    "_id" : ObjectId("628e015044f9acaee0e60079"),
    "name" : "Kiwi",
    "color" : "Green",
    "stock" : 25.0,
    "price" : 50.0,
    "supplier_id" : 1.0,
    "onSale" : true,
    "origin" : [ 
        "US", 
        "China"
    ]
}

/* 4 */
{
    "_id" : ObjectId("628e015044f9acaee0e6007a"),
    "name" : "Mango",
    "color" : "Yellow",
    "stock" : 10.0,
    "price" : 120.0,
    "supplier_id" : 2.0,
    "onSale" : false,
    "origin" : [ 
        "Philippines", 
        "India"
    ]
}

//2
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "Number of fruits on sale" }
]);

//3
db.fruits.aggregate([
    { $match: { stock: { $gt: 20 } } },
    { $count: "Number of fruits with stock more than 20" }
]);
//4
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "_id", AveragePrice: { $avg: "$price" } } }
]);
//5
db.fruits.aggregate([
    { $group: { _id: "$supplier_id", MaxPrice: { $max: "$price" } } }
]);
//6
db.fruits.aggregate([
    { $group: { _id: "$supplier_id", MinPrice: { $min: "$price" } } }
]);

